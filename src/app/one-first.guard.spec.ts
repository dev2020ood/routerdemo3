import { TestBed } from '@angular/core/testing';

import { OneFirstGuard } from './one-first.guard';

describe('OneFirstGuard', () => {
  let guard: OneFirstGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(OneFirstGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
