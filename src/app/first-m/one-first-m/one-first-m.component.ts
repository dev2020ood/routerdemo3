import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';

@Component({
  selector: 'app-one-first-m',
  templateUrl: './one-first-m.component.html',
  styleUrls: ['./one-first-m.component.css']
})
export class OneFirstMComponent implements OnInit {

  id:string
  constructor(private route:ActivatedRoute) { 
     this.route.params.subscribe(value=>this.id = value.id)
  }

  ngOnInit(): void {
  }

}
