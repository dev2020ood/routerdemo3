import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OneFirstMComponent } from './one-first-m.component';

describe('OneFirstMComponent', () => {
  let component: OneFirstMComponent;
  let fixture: ComponentFixture<OneFirstMComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OneFirstMComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OneFirstMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
