import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OneFirstMComponent } from './one-first-m/one-first-m.component';
import { TwoFirstMComponent } from './two-first-m/two-first-m.component';
import { FirstGuardGuard } from '../first-guard.guard';
import { OneFirstGuard } from '../one-first.guard';

const routes: Routes = [
  {path:"One/:id",component:OneFirstMComponent,canActivate:[OneFirstGuard]},
  {path:"Two",component:TwoFirstMComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FirstMRoutingModule { }
