import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FirstMRoutingModule } from './first-m-routing.module';
import { OneFirstMComponent } from './one-first-m/one-first-m.component';
import { TwoFirstMComponent } from './two-first-m/two-first-m.component';


@NgModule({
  declarations: [OneFirstMComponent, TwoFirstMComponent],
  imports: [
    CommonModule,
    FirstMRoutingModule
  ]
})
export class FirstMModule { }
