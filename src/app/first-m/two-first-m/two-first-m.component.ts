import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-two-first-m',
  templateUrl: './two-first-m.component.html',
  styleUrls: ['./two-first-m.component.css']
})
export class TwoFirstMComponent implements OnInit {

  constructor(private route:ActivatedRoute,private router:Router,private location:Location) { 
    this.route.queryParams.subscribe(value=>console.log(value))
    console.log(router.url)
    this.router.events.subscribe(event=>console.log(event))
    
  }

  ngOnInit(): void {
    
      }
  NavTo(){
    this.router.navigate(['firstmodule/One',1])

  }

}
