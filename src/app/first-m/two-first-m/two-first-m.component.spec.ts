import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoFirstMComponent } from './two-first-m.component';

describe('TwoFirstMComponent', () => {
  let component: TwoFirstMComponent;
  let fixture: ComponentFixture<TwoFirstMComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TwoFirstMComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TwoFirstMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
