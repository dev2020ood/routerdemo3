import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FirstComponent } from './first/first.component';
import { HomeComponent } from './home/home.component';
import { SecondComponent } from './second/second.component';

const routes: Routes = [
  {path:"", component:HomeComponent},
  {path:"first", component:FirstComponent},
  {path:"second",component:SecondComponent},
  {path:"firstmodule", loadChildren: () => import('../app/first-m/first-m.module').then(m => m.FirstMModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
